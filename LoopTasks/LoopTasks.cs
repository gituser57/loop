using System;

namespace LoopTasks
{
    public static class LoopTasks
    {
        public static int SumOfOddDigits(int n)
        {
            //this method should return the sum of the odd digits of n.
            
            int result = 0;
            string stringN = n.ToString();
            
            for (int i = 0; i < stringN.Length; i++)
            {
                if ((int)Char.GetNumericValue(stringN[i]) % 2 != 0)
                {
                    result += (int)Char.GetNumericValue(stringN[i]);
                }
            }
            return result;
        }

        public static int NumberOfUnitsInBinaryRecord(int n)
        {
            //this method should return the number of units in the binary notation of n.
            
            string binaryN = Convert.ToString(n, 2);

            var result = 0;

            for (int i = 0; i < binaryN.Length; i++)
            {
                if (binaryN[i] == '1')
                {
                    result++;
                }
            }

            return result;
        }

        public static int SumOfFirstNFibonacciNumbers(int n)
        {
            //this method should return the sum of the first n Fibonacci numbers.

            int result = 0;

            for (int i = 1; i <= n; i++)
            {
                result += Fibonacci(i);
            }

            return result;
        }

        private static int Fibonacci(int n)
        {
            int a = 0;
            int b = 1;
            int tmp;
 
            for (int i = 0; i < n; i++)
            {
                tmp = a;
                a = b;
                b += tmp;
            }
 
            return a;
        }
    }
}